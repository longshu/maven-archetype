# 自定义maven-archetype #

- 重新整理至 [http://git.oschina.net/longshu/maven-archetypes](http://git.oschina.net/longshu/maven-archetypes)

1. 用SVN/Git客户端检出项目  :fa-plus-square: 

```
SVN: svn://git.oschina.net/longshu/maven-archetype
Git: https://git.oschina.net/longshu/maven-archetype.git
```
2. 选择要安装的archetype,比如webapp,运行install.bat
3. archetype_crawl.bat,详细看 [自定义maven-archetype.txt](自定义maven-archetype.txt) 第3-4步

这些项目骨架免去了maven项目目录结构的创建,Web项目添加了Servlet2.5-3.1的版本,并且添加了WEB-INF/lib本地编译依赖路径。

**以下来至原版的修改:**
> quickstart
> webapp

## 下面是创建结果 ##
1. **控制台下:**
![*控制台下](images/maven-archetype_1.png)
2. **Eclipse下:**
![Eclipse下](images/maven-archetype_2.png)
![Eclipse下](images/maven-archetype_3.png)
